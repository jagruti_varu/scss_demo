import React from 'react';
import PropTypes from 'prop-types';
import Error from './Error';

export default class Input extends React.Component {
    static propTypes = {
        wrapperClass: PropTypes.string,
        labelText: PropTypes.string,
        labelClass: PropTypes.string,
        columnClass: PropTypes.string,
        name: PropTypes.string,
        placeholder: PropTypes.string,
        type: PropTypes.string,
        disabled: PropTypes.bool,
        onFocus: PropTypes.func,
        onBlur: PropTypes.func,
        onChange: PropTypes.func,
        tabIndex: PropTypes.string,
        readOnly: PropTypes.bool,
        autoFocus: PropTypes.bool,
        value: PropTypes.any,
        min: PropTypes.string,
        required: PropTypes.bool,
        error: PropTypes.string,
        floating: <PropTypes className="bool"></PropTypes>
    };
    static defaultProps = {
        wrapperClass: 'form-group',
        labelClass: 'control-label',
        columnClass: '',
        type: 'text',
        floating: false,
        min: '',
        onFocus() {},
        onBlur() {},
        onChange() {}
    };

    constructor(props) {
        super(props);
    }

    focus() {
        this.input.focus();
    }

    blur() {
        this.input.blur();
    }

    handleChange = e => {
        const { props } = this;
        if (props.disabled) {
            return;
        }
        let value = e.target.value;
        //let selectedValue = (props.type === 'number') ? ((value) ? value : 0) : value;
        props.onChange({
            target: {
                ...props,
                value: value
            },
            stopPropagation() {
                e.stopPropagation();
            },
            preventDefault() {
                e.preventDefault();
            }
        });
    };

    saveInput = node => {
        this.input = node;
    };

    render() {
        const {
            wrapperClass,
            labelText,
            labelClass,
            columnClass,
            name,
            placeholder,
            type,
            disabled,
            readOnly,
            tabIndex,
            onFocus,
            onBlur,
            autoFocus,
            value,
            error,
            min,
            required,
            floating,
            baseInfoText
        } = this.props;

        let  colorClass = this.props.type === 'color' ? ' jscolor':  '';
        const classString = (error ? 'is-invalid form-control' : 'form-control') + colorClass;

        return (
            <div className={wrapperClass}>

                <div className={columnClass}>

                    {labelText && !floating ? (
                        <label className={labelClass} htmlFor={name}>
                            {labelText}
                            {required && <span className="required text-danger">*</span>}
                        </label>
                    ) : (
                        ''
                    )}

                    <input
                        name={name}
                        type={type}
                        readOnly={readOnly}
                        disabled={disabled}
                        tabIndex={tabIndex}
                        onFocus={onFocus}
                        onBlur={onBlur}
                        onChange={this.handleChange}
                        autoFocus={autoFocus}
                        ref={this.saveInput}
                        value={value}
                        placeholder={placeholder}
                        required={required}
                        className={classString}
                        min={min}
                        id={name}
                    />
                    {labelText && floating ? (
                        <label className={labelClass} htmlFor={name}>
                            {labelText}
                            {required && <span className="required"/>}
                        </label>
                    ) : (
                        ''
                    )}
                    {baseInfoText ? (
                        <p className="">
                            { baseInfoText }
                        </p>
                    ) : null
                    }
                    {error && <Error error={error} />}
                </div>
            </div>
        );
    }
}
