import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { observer } from 'mobx-react';
import PageStore from '../store/PageStore';

class AdminApp extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let currentPage = this.props.pageStore.currentPage;
        let pageComponent = React.createElement(currentPage.component, {
            pageStore: this.props.pageStore,
            page: currentPage
        });

        return (
            <div className="main-wrap">
                <Sidebar page={currentPage} pageStore={pageStore}/>
                <div className="">
                    <TopNavigationBar page={currentPage} pageStore={pageStore} />
                    <div className="content-wrap">
                        {pageComponent}
                    </div>
                </div>
                <div>
                    <Notifications />
                </div>
            </div>
        );
    }
}

if (document.getElementById('root')) {
    ReactDOM.render(<AdminApp pageStore={pageStore} />, document.getElementById('root'));
}
