import React, { Component } from 'react';
import { createStore } from 'redux';
import { Provider } from 'react-redux'
import './App.scss';
import appState from './reducers';
import Counter from './components/Counter'

import {
    BrowserRouter as Router
} from 'react-router-dom';

const store = createStore(
    appState
);

class App extends Component {
    render() {
        return (
            <Router>
                <Provider store={store}>
                    <div>
                        <Counter/>
                     </div>
                </Provider>
            </Router>
        );
    }
}

export default App;
