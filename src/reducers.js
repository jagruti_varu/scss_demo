// import {arrayMove} from 'react-sortable-hoc';

import {
    COUNTER_MINUS,
    COUNTER_PLUS
} from './actions';

const initialState = {
    clock: {
        counter: 0,
    }
};

export default function appState(state = initialState, action) {
    console.log(action.type);
    switch (action.type) {
        case COUNTER_MINUS:
            return Object.assign({}, state, {
                clock: {
                    ...state.clock,
                    counter: state.clock.counter - 1,
                }
            });

        case COUNTER_PLUS:
            console.log(state.clock.counter);
            return Object.assign({}, state, {
                clock: {
                    ...state.clock,
                    counter: state.clock.counter + 1,
                }
            });


        default:
            if (action.type.endsWith('FAILURE')) {
                if (action.response && action.response.status === 401) {
                    localStorage.removeItem('jwt');

                    return Object.assign({}, state, {
                        ...state,
                        auth: {
                            ...state.auth,
                            apiKey: null,
                            user: null,
                        }
                    });
                }

                if (action.response && action.response.data && action.response.data.message) {
                    alert(action.response.data.message);
                }
            }

            return state;
    }
}
