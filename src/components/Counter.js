import React, {Component} from 'react';
import {connect} from 'react-redux';
import 'bootstrap/dist/css/bootstrap.min.css';

import {counter_plus, counter_minus} from './../actions';


const mapStateToProps = state => {
    return {
        clock: state.clock,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        counter_plus: (counter) => {
            dispatch(counter_plus(counter))
        },
        counter_minus: (counter) => {
            dispatch(counter_minus(counter))
        }
    }
};

class Counter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            counter: 1
        };
    }

    onClickPlus() {
        this.props.counter_plus(this.state.counter);
    }

    onClickMinus() {
        this.props.counter_minus(this.state.counter);
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12 text-center">
                    <div>
                        <h1> Counter : {this.props.clock.counter}</h1>
                    </div>
                    <div className="form-action">
                        <button
                            className="btn btn-primary"
                            onClick={this.onClickPlus.bind(this)}>
                            Plus
                        </button>
                        <button
                            className="btn btn-primary m-5"
                            onClick={this.onClickMinus.bind(this)}>
                            Minus
                        </button>
                    </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Counter);