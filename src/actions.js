
//Increment counter
const COUNTER = 'COUNTER';
export const COUNTER_PLUS = 'COUNTER_PLUS';

export function counter_plus(counter) {
    console.log(counter);
    return {
            type: COUNTER_PLUS,
            counter: {counter}

    };
}

//Decrement counter
export const COUNTER_MINUS = 'COUNTER_MINUS';

export function counter_minus(counter) {
    return {
        type: COUNTER_MINUS,
        counter:counter

    };
}

